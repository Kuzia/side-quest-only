﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    public static PrefabManager instance;

    public GameObject itemPrefab;
    public GameObject nPCPrefab;
    public GameObject syncPrefab;
    public GameObject[] npcs;
    public GameObject[] items;

    public static GameObject ItemPrefab 
    {
        get { return instance.itemPrefab; }
    }

    public static GameObject SyncPrefab {
        get { return instance.syncPrefab; }
    }

    public static GameObject NPCPrefab
    {
        get { return instance.nPCPrefab; }
    }
    
    private void OnEnable() {
        instance = this;
    }
    
 
}
