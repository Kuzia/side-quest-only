using UnityEngine;
using UnityEngine.AI;

namespace Mirror.Examples.Tanks
{
    public class Tank : NetworkBehaviour
    {
        // Picking Up
        [Header("Firing")]
        public KeyCode shootKey = KeyCode.Space;
    
        public Transform useTransform;

        [Header("Game Stats")]
        [SyncVar]
        public int health;
        [SyncVar]
        public int score;
        [SyncVar]
        public string playerName;
        [SyncVar]
        public bool allowMovement;
        [SyncVar]
        public bool isReady;

        public bool isDead => health <= 0;
        public TextMesh nameText;

        // horizontal rotation speed
        public float horizontalSpeed = 1f;
        // vertical rotation speed
        public float verticalSpeed = 1f;
        
        private float xRotation = 0.0f;
        private float yRotation = 0.0f;
        private Camera cam;
        private Quaternion cameraRotation = new Quaternion();

        public Projectile projectile = null;

        void UpdateCamera() {
            Vector3 position = transform.position;
            position.y += 3;

            Camera.main.transform.position = position;
            Camera.main.transform.rotation = cameraRotation;
        }

        private void Start() {
            // Host is spawning items/npc
            if (isServer && isLocalPlayer) {
                foreach(ItemSpawnPoint p in ItemSpawnPoint.List) {
                 
                    GameObject projectile = Instantiate(PrefabManager.ItemPrefab, p.transform.position, p.transform.rotation);
                    //projectile.transform.position = p.transform.position;

                    Projectile pro = projectile.GetComponent<Projectile>();
                    pro.source = gameObject;
                    pro.type = (int)p.type;

                    NetworkServer.Spawn(projectile);
                }

                foreach(NPCSpawnPoint npc  in NPCSpawnPoint.List) {
                 
                    GameObject npcGameObject = Instantiate(PrefabManager.NPCPrefab, npc.transform.position, npc.transform.rotation);

                    npcGameObject.GetComponent<NPCController>().look = Random.Range(0, 4);

             
                    NetworkServer.Spawn(npcGameObject);
                }

     
       
                foreach(DoorController door in DoorController.List) {
                    GameObject syncPrefab = Instantiate(PrefabManager.SyncPrefab);

                    NetworkBehaviourPrefab netPrefab = syncPrefab.GetComponent<NetworkBehaviourPrefab>();
                    door.netPrefab = netPrefab;
                    door.isServer = true;

                    syncPrefab.transform.parent = door.transform;
                    syncPrefab.transform.localPosition = Vector3.zero;
                    syncPrefab.transform.localRotation = Quaternion.Euler(0,0,0);
                    syncPrefab.transform.localScale = Vector3.one;

                    door.syncObject = syncPrefab;

                    netPrefab.serverOwner = DoorController.List.IndexOf(door);
    
                    NetworkServer.Spawn(syncPrefab);
                }
                
            }
        }

        void LateUpdate() {
            // local player use button position update
            if (isLocalPlayer) {
                   // Update Camera Transform
                Transform cameraTransform = Camera.main.transform;
                
                Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);

                useTransform.position = cameraTransform.position + forward * 3;
            }

            // If client, send position to the server
            if (!isServer) {
                CmdPos(useTransform.position);
            }

            // If server & player have item, update item position and velocity
            if (isServer) {
                if (projectile != null) {
                    projectile.transform.position = useTransform.position;
                    projectile.rigidBody.velocity = Vector3.zero;
                    projectile.rigidBody.angularVelocity = Vector3.zero;
                }
            }
        }

        // Can player grab projectile?
        public Projectile GetProjectile() {
            Projectile projectile = null;
            foreach(Projectile p in Projectile.List) {
                float distance = Vector3.Distance(p.transform.position, useTransform.position);
                
                if (distance < 4) {
                    projectile = p;
                }
            }
            return(projectile);
        }

          public NPCController GetNPC() {
            NPCController npc = null;
            foreach(NPCController p in NPCController.List) {
                float distance = Vector3.Distance(p.transform.position, useTransform.position);
                
                if (distance < 4) {
                    npc = p;
                }
            }
            return(npc);
        }

        // Can player open doors?
        public DoorController GetDoors() {
           DoorController door = null;
            foreach(DoorController d in DoorController.List) {
                float distance = Vector3.Distance(d.transform.position, useTransform.position);
                
                if (distance < 3) {
                    door = d;
                }
            }
            return(door);
        }


        // 3d person movement
        void UpdateMovement() {
            cam = Camera.main;

            float mouseX = Input.GetAxis("Mouse X") * horizontalSpeed;
            float mouseY = Input.GetAxis("Mouse Y") * verticalSpeed;
    
            yRotation += mouseX;
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
    
            cameraRotation.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);

            transform.rotation = Quaternion.Euler(0,  yRotation, 0);

            // move
            float vertical = Input.GetAxis("Vertical");

            Transform cameraTransform = Camera.main.transform;

            Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
            forward.y = 0;
            forward = forward.normalized;

            Vector3 back = cameraTransform.TransformDirection(Vector3.back);
            back.y = 0;
            back = back.normalized;

            Vector3 left = cameraTransform.TransformDirection(Vector3.left);
            Vector3 right = cameraTransform.TransformDirection(Vector3.right);

            Rigidbody rigidbody = GetComponent<Rigidbody>();
                
            bool moving = false;
            float speed = 10;
            Vector3 velocity = Vector3.zero;

            if (Input.GetKey(KeyCode.W)) {
            
                velocity = forward * speed;
                moving = true;

            } else if (Input.GetKey(KeyCode.S)) {
              
                velocity = back * speed;
                moving = true;

            }

            if (Input.GetKey(KeyCode.A)) {
            
                velocity = left * speed;
                moving = true;

            } else if (Input.GetKey(KeyCode.D)) {
            
                velocity = right * speed;
                moving = true;

            }

            if (Input.GetKeyDown(KeyCode.Space)) {

                rigidbody.velocity += new Vector3(0, 10, 0);
           
            }

            if (moving ) {
                Vector3 v = (velocity);
                v.y = rigidbody.velocity.y;
                rigidbody.velocity = v;

            } else {
                Vector3 v = rigidbody.velocity;

                v.x = 0;
                v.z = 0;

                rigidbody.velocity = v;
            }
        }

        void Update()
        {
            if(Camera.main)
            {
                nameText.text = playerName;
                nameText.transform.rotation = Camera.main.transform.rotation;
            }
        
            if (!isLocalPlayer) {
                return;
            } else {
                nameText.gameObject.SetActive(false);
            } 

            nameText.color = Color.green;

            if (!allowMovement)
                return;

            if (isDead)
                return;

            UpdateMovement();

            UpdateCamera();
        
            // grab item
            if (Input.GetKeyDown(KeyCode.E))
            {
                CmdGrab();
            }
            // grab item
            if (Input.GetKeyDown(KeyCode.F))
            {
                CmdOpen();
            }
        }

        // this is called on the server
        [Command]
        void CmdGrab()
        {
            bool drop = false;
            if (projectile != null) {
                drop = true;
            } else {
                projectile = GetProjectile();
               // projectile.rigidBody.isKinematic = true;
            }

            NPCController nPC = GetNPC();

            if (nPC != null) {
                NPCChild child = nPC.npcChild;

                if (child != null) {
                    int typeId = (int)child.id;
                    
                    if (child.item.gameObject.activeSelf) {
                        child.item.gameObject.SetActive(false);

                        Transform t = child.item.transform;

                        GameObject item = Instantiate(PrefabManager.ItemPrefab, t.position,t.rotation);
                        //projectile.transform.position = p.transform.position;

                        Projectile pro = item.GetComponent<Projectile>();
                        pro.source = gameObject;
                        pro.type = typeId;

                      

                        projectile = pro;
                         //projectile.rigidBody.isKinematic = true;

                        NetworkServer.Spawn(item);
                    } else {
                        if (projectile != null && projectile.type == typeId) {
                            Destroy(projectile.gameObject);

                            child.item.gameObject.SetActive(true);
                        }
                    }

                    
                    

                  
                }
            }

            if (drop) {
                projectile.rigidBody.isKinematic = false;
                projectile = null;
                
            }

           

            RpcOnGrab();
        }

        [Command]
        void CmdOpen()
        {
            DoorController door = GetDoors();
            if (door != null){
                door.TriggerDoor();
            }

            RpcOnGrab();
        }

        [Command]
        void CmdPos(Vector3 v)
        {
            Debug.Log("receive" + v);
            useTransform.position = v;

            RpcPos();
        }

        [ClientRpc]
        void RpcPos()
        {
           
        }

        // this is called on the tank that fired for all observers
        [ClientRpc]
        void RpcOnGrab()
        {
           
        }

        public void SendReadyToServer(string playername)
        {
            if (!isLocalPlayer)
                return;

            CmdReady(playername);
        }

        [Command]
        void CmdReady(string playername)
        {
            if (string.IsNullOrEmpty(playername))
            {
                playerName = "PLAYER" + Random.Range(1, 99);
            }
            else
            {
                playerName = playername;
            }

            isReady = true;
        }
    }
}
