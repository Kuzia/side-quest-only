using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace Mirror.Examples.Tanks
    {
    public class DoorController : MonoBehaviour
    {
        public static List<DoorController> List = new List<DoorController>();

        public enum Type {Doors, ClosetDoors}

        public Type type = Type.Doors;

        public enum RotationType {Clockwise, AnticlockWise}

        public RotationType rotationType = RotationType.Clockwise;

        public GameObject syncObject;
        public NetworkBehaviourPrefab netPrefab;

        public bool isServer = false;

        public int state = 0;
        public AudioClip doorOpen;
        public AudioClip doorClose;

        private float rotation = 0;
        private AudioSource aSource;

        private void Awake()
        {
            aSource = GetComponent<AudioSource>();
        }
        private void OnEnable() {
            List.Add(this);
        }

        private void Update() {
            if (syncObject == null) {
                return;
            }

            if (isServer == false) {
                transform.localRotation = netPrefab.transform.localRotation;
                return;
            }
           
            if (state == 1) {
                rotation = rotation * .95f + 90 * 0.05f;
            }

            if (state == 0) {
                rotation = rotation * .95f;
            }

            float setRotation = rotation;

            if (rotationType == RotationType.AnticlockWise) {
                setRotation = -setRotation;
            }

            switch(type) {
                case Type.Doors:
                    transform.localRotation = Quaternion.Euler(0, 0, setRotation);
                break;

                case Type.ClosetDoors:
                    
                    transform.localRotation = Quaternion.Euler(0, setRotation, 0);
                break;
            }

            netPrefab.transform.localRotation = transform.localRotation;

        }

        public void TriggerDoor() {
            if (state == 0) {
                aSource.PlayOneShot(doorOpen);
                state = 1;
            } else {
                aSource.PlayOneShot(doorClose);
                state = 0;
            }
        }


    }
}