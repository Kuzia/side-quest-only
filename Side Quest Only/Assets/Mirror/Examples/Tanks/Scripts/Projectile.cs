using UnityEngine;
using System.Collections.Generic;

namespace Mirror.Examples.Tanks
{
    public class Projectile : NetworkBehaviour
    {
        public float destroyAfter = 1;
        public Rigidbody rigidBody;

        public static List<Projectile> List = new List<Projectile>();

        [Header("Game Stats")]
        public int damage;
        public GameObject source;

        [SyncVar]
        public int type = 0;

     
        public override void OnStartServer()
        {
            //Invoke(nameof(DestroySelf), destroyAfter);
        }

        private void OnDestroy() {
            List.Remove(this);
        }

        // set velocity for server and client. this way we don't have to sync the
        // position, because both the server and the client simulate it.
        void Start()
        {
            List.Add(this);
            //rigidBody.AddForce(transform.forward * force);

            GameObject g = Instantiate(PrefabManager.instance.items[type]);
         
            g.transform.parent = transform;

            g.transform.localPosition = Vector3.zero;
            g.transform.localRotation = Quaternion.Euler(0, 0, 0);
            g.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }

        private void Update() {
            if (isServer) {
                rigidBody.AddForce(new Vector3(0, -5, 0));
            } else {
                rigidBody.isKinematic = true;
                gameObject.GetComponent<Collider>().isTrigger = true;
            }
            
        }

        // destroy for everyone on the server
        [Server]
        void DestroySelf()
        {
           // NetworkServer.Destroy(gameObject);
        }

        // ServerCallback because we don't want a warning if OnTriggerEnter is
        // called on the client
        [ServerCallback]
        void OnTriggerEnter(Collider co)
        {
            //Hit another player
            if (co.tag.Equals("Player") && co.gameObject != source)
            {
                //Apply damage
                co.GetComponent<Tank>().health -= damage;

                //update score on source
                source.GetComponent<Tank>().score += damage;
            }

           //NetworkServer.Destroy(gameObject);
        }
    }
}
