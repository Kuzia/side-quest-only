﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mirror.Examples.Tanks
{
    public class NPCController : NetworkBehaviour
    {
        public static List<NPCController> List = new List<NPCController>();

        public NPCChild npcChild;


        private void OnEnable() {
            List.Add(this);
        }

        private void OnDestroy() {
            List.Remove(this);
        }

        [SyncVar]
        public int look = 0;    

        public void Start() {
            GameObject g = Instantiate(PrefabManager.instance.npcs[look]);
            npcChild = g.GetComponent<NPCChild>();

            g.transform.parent = transform;

            g.transform.localPosition = Vector3.zero;
            g.transform.localRotation = Quaternion.Euler(0, 0, 0);
            g.transform.localScale = Vector3.one;

        }
    }
}