﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Mirror.Examples.Tanks
{
   
    public class NetworkBehaviourPrefab : NetworkBehaviour
    {
        [SyncVar]
        public int serverOwner;

        public GameObject gObject;

        public void Update() {
            gObject = GetGameObject();

            transform.parent = gObject.transform;

            gObject.transform.localRotation = transform.localRotation;
        }

        public GameObject GetGameObject() {
            return(DoorController.List[serverOwner].gameObject);
        }
           
    
    }
}