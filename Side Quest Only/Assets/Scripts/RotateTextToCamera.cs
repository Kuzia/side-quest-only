using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTextToCamera : MonoBehaviour
{

    void Update() {
       // transform.LookAt(Camera.main.transform, Vector3.forward);

       transform.rotation = Quaternion.LookRotation( transform.position - Camera.main.transform.position );

    }
}
