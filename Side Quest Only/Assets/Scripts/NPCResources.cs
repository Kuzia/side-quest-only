﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCResources : MonoBehaviour
{
    public AudioClip stepSound;
    public AudioClip[] speakSound;
    public int maxSpeakTimer = 1000;
    private int speakTimer;
    private Animator anim;
    private AudioSource aSource;
    private Vector3 lastPos;


    // Start is called before the first frame update
    private void Awake()
    {
        aSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        lastPos = transform.position;
        speakTimer = Random.Range(150, maxSpeakTimer);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, lastPos) > 0.1f)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }
        lastPos = transform.position;
        speakTimer--;
        if (speakTimer <= 0)
        {
            Speak();
            speakTimer = Random.Range(150, maxSpeakTimer);
        }
    }

    public void StepSound()
    {
        aSource.PlayOneShot(stepSound);
    }

    public void Speak()
    {
        aSource.clip = speakSound[Random.Range(0, speakSound.Length)];
        aSource.Play();
    }
}
