using UnityEngine;
using UnityEngine.AI;

namespace Mirror.SideQuestOnly
    {
    public class PlayerController : NetworkBehaviour
    {
        [Header("Components")]
        public Animator animator;

        [Header("Movement")]
        public float rotationSpeed = 100;

        [Header("Firing")]
        public KeyCode shootKey = KeyCode.Space;
        public GameObject projectilePrefab;
        public Transform projectileMount;

        public Transform useTransform;

        [Header("Game Stats")]
        [SyncVar]
        public int health;
        [SyncVar]
        public int score;
        [SyncVar]
        public string playerName;
        [SyncVar]
        public bool allowMovement;
        [SyncVar]
        public bool isReady;

        public bool isDead => health <= 0;
        public TextMesh nameText;

          // horizontal rotation speed
        public float horizontalSpeed = 1f;
        // vertical rotation speed
        public float verticalSpeed = 1f;
        private float xRotation = 0.0f;
        private float yRotation = 0.0f;
        private Camera cam;



        Quaternion cameraRotation = new Quaternion();


        void UpdateCamera() {
            Vector3 position = transform.position;
            position.y += 2;

            Camera.main.transform.position = position;
            Camera.main.transform.rotation = cameraRotation;
        }

        void UpdateMovement() {
            // rotate
            //float horizontal = Input.GetAxis("Horizontal");
            //transform.Rotate(0, horizontal * rotationSpeed * Time.deltaTime, 0);

            // Camera 3rd Person Movement

            cam = Camera.main;

            float mouseX = Input.GetAxis("Mouse X") * horizontalSpeed;
            float mouseY = Input.GetAxis("Mouse Y") * verticalSpeed;
    
            yRotation += mouseX;
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
    
            cameraRotation.eulerAngles = new Vector3(xRotation, yRotation, 0.0f);

            // move
            float vertical = Input.GetAxis("Vertical");

            Transform cameraTransform = Camera.main.transform;

            Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
            Vector3 back = cameraTransform.TransformDirection(Vector3.back);
            Vector3 left = cameraTransform.TransformDirection(Vector3.left);
            Vector3 right = cameraTransform.TransformDirection(Vector3.right);

            useTransform.position = cameraTransform.position + forward;
           
            Rigidbody rigidbody = GetComponent<Rigidbody>();
                
            bool moving = false;

            Vector3 velocity = Vector3.zero;

            float speed = 1;

            if (Input.GetKey(KeyCode.W)) {
            
                velocity = forward * speed;
                moving = true;

            } else if (Input.GetKey(KeyCode.S)) {
              
                velocity = back * speed;
                moving = true;

            }

            if (Input.GetKey(KeyCode.A)) {
            
                velocity = left * speed;
                moving = true;

            } else if (Input.GetKey(KeyCode.D)) {
            
                velocity = right * speed;
                moving = true;

            }

            if (Input.GetKeyDown(KeyCode.Space)) {

                velocity.y = 5;
                moving = true;

            }

            if (moving ) {
                Vector3 v = (rigidbody.velocity + velocity);
                rigidbody.velocity = v;

            } else {
                Vector3 v = rigidbody.velocity;

                v.x = 0;
                v.z = 0;

                rigidbody.velocity = v;
            }

           // animator.SetBool("Moving", agent.velocity != Vector3.zero);
        }

        void Update()
        {
            if(Camera.main)
            {
                nameText.text = playerName;
                nameText.transform.rotation = Camera.main.transform.rotation;
            }

            
            
            // movement for local player
            if (!isLocalPlayer) {
                return;
            } else {
                nameText.gameObject.SetActive(false);
            }
                

            //Set local players name color to green
            nameText.color = Color.green;

            if (!allowMovement)
                return;

            if (isDead)
                return;

            UpdateMovement();

            UpdateCamera();
        
            // shoot
            // if (Input.GetKeyDown(shootKey))
            // {
            //    CmdFire();
            // }
        }

        // this is called on the server
        [Command]
        void CmdFire()
        {
            GameObject projectile = Instantiate(projectilePrefab, projectileMount.position, transform.rotation);
          //  projectile.GetComponent<Projectile>().source = gameObject;
            NetworkServer.Spawn(projectile);
            RpcOnFire();
        }

        // this is called on the tank that fired for all observers
        [ClientRpc]
        void RpcOnFire()
        {
            animator.SetTrigger("Shoot");
        }

        public void SendReadyToServer(string playername)
        {
            if (!isLocalPlayer)
                return;

            CmdReady(playername);
        }

        [Command]
        void CmdReady(string playername)
        {
            if (string.IsNullOrEmpty(playername))
            {
                playerName = "PLAYER" + Random.Range(1, 99);
            }
            else
            {
                playerName = playername;
            }

            isReady = true;
        }
    }
}
