﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharResources : MonoBehaviour
{
    public AudioClip stepSound;
    private Animator anim;
    private AudioSource aSource;
    private Vector3 lastPos;

    // Start is called before the first frame update
    private void Awake()
    {
        aSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        lastPos = transform.position;
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, lastPos) > 0.1f)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }
        lastPos = transform.position;
    }

    public void StepSound()
    {
        aSource.PlayOneShot(stepSound);
    }
}
